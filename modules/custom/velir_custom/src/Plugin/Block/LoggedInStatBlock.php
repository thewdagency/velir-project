<?php
/**
 * @file
 * Contains \velir_custom\Plugin\Block\LoggedInStatBlock
 */
namespace Drupal\velir_custom\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a velir custom logged in status block.
 *
 * @Block(
 *   id = "velir_custom_logged_in_stat_block",
 *   admin_label = @Translation("Velir Logged In Stat Block"),
 *   category = @Translation("Velir Custom"),
 * )
 */
class LoggedInStatBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $userService = \Drupal::service('velir_custom.users');
    $username = $userService->getUsername();

    return [
      '#theme' => 'velir_custom_logged_in_stat_block',
      '#content' => $username === 'Anonymous' ? 'Log In' : 'Welcome ' . $username,
      // Use the user cache context since we are dependent on the username
      '#cache' => [
        'contexts' => [
          'user',
        ],
      ],
    ];
  }
}
