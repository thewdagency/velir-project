<?php

namespace Drupal\velir_custom\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'velir_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "velir_field_formatter",
 *   label = @Translation("Format to UPPERCASE"),
 *   field_types = {
 *     "string"
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class UppercaseFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    // Text to show after selecting this formatter
    $summary[] = t('Converts the entered string to uppercase');

    return $summary;
  }

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param $langcode
   *
   * @return array
   */
  public function viewElements( FieldItemListInterface $items, $langcode ): array {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => '{{ value|upper }}',
        '#context' => ['value' => $item->value],
      ];
    }

    return $elements;
  }
}
