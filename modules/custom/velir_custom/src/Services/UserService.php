<?php
/**
 * @file
 * Contains \velir_custom\Services\UserService
 *
 * Usage Example:
 *    $roles = \Drupal::service('velir_custom.users');
 */
namespace Drupal\velir_custom\Services;

use Drupal\Core\Session\AccountProxy;
use Drupal\velir_custom\Utilities\UserData;

class UserService extends UserData {

  public function __construct(AccountProxy $currentUser) {
    parent::__construct($currentUser);
  }

  public function getUsername() {
    return $this->getCurrentUser()->getDisplayName();
  }
}
