<?php

namespace Drupal\velir_custom\Normalizer;

use Drupal\serialization\Normalizer\ContentEntityNormalizer;

class NodeEntityNormalizer extends ContentEntityNormalizer {

  public function normalize( $entity, $format = NULL, array $context = [] ): array {
    $data = parent::normalize($entity, $format, $context);

    $data['velir'][]['value'] = 212;

    return $data;
  }

}
