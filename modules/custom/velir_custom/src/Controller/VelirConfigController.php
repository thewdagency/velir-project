<?php
/**
 * @file
 * Contains \velir_custom\Controller\VelirConfigController
 */

namespace Drupal\velir_custom\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines VelirConfigController class.
 */
class VelirConfigController extends ControllerBase {

  public function namer(): array {
    return [
      '#title' => 'Velir Namer Config Settings',
      'config_form' => \Drupal::formBuilder()->getForm('Drupal\velir_custom\Forms\VelirNamerConfigForm')
    ];
  }

}
