<?php
/**
 * @file
 * Contains \velir_custom\Controller\VelirMainController
 */

namespace Drupal\velir_custom\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines VelirMainController class.
 */
class VelirMainController extends ControllerBase {

  public function adminMain(): array {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Velir Custom Admin Panel'),
    ];
  }

  /**
   * Return data for 'hello-velir-1'
   *
   * @return array
   */
  public function route1(): array {
    $userService = \Drupal::service('velir_custom.users');
    $username = $userService->getUsername();

    return [
      '#theme' => 'velir_custom_route1',
      '#attached' => [
        'library' => [
          'velir_custom/velir_custom_css',
          'velir_custom/velir_custom_js',
        ]
      ],
      '#name' => $username,
      // Use the user cache context since we are dependent on the username
      '#cache' => [
        'contexts' => [
          'user',
        ],
      ],
    ];
  }

  /**
   * Return data for 'hello-velir-2'
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function route2(): JsonResponse {
    $userService = \Drupal::service('velir_custom.users');
    $username = $userService->getUsername();

    return new JsonResponse([ 'data' => [ 'name' => $username, 'fullText' => 'Hello, my name is ' . $username ], 'method' => 'GET',
    'status'=> 200]);
  }

  /**
   * Return data for 'hello-velir-3'
   *
   * @return array
   */
  public function route3(): array {
    $userService = \Drupal::service('velir_custom.users');
    $username = $userService->getUsername();

    return [
      '#theme' => 'velir_custom_route3',
      '#attached' => [
        'library' => [
          'velir_custom/velir_custom_css',
          'velir_custom/velir_custom_js',
        ]
      ],
      '#name' => $username,
      // Use the user cache context since we are dependent on the username
      '#cache' => [
        'contexts' => [
          'user',
        ],
      ],
    ];
  }

}
