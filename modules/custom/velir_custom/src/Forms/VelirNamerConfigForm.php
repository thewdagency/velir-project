<?php
/**
 * @file
 * Contains \velir_custom\Form\VelirNamerConfigForm
 */
namespace Drupal\velir_custom\Forms;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class VelirNamerConfigForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'velir_custom_site_settings';
  }

  /**
   * Build the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm( array $form, FormStateInterface $form_state ) {
    $config = \Drupal::config('velir_custom_namer.settings');

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Name'),
    ];

    $form['settings']['first_name'] = [
      '#type'   => 'textfield',
      '#title'  => t('First Name'),
      '#description' => t('Enter a first name for the config'),
      '#default_value' => $config->get('first_name')
    ];

    $form['settings']['last_name'] = [
      '#type'   => 'textfield',
      '#title'  => t('Last Name'),
      '#description' => t('Enter a last name for the config'),
      '#default_value' => $config->get('last_name')
    ];

    $form['settings']['actions'] = [
      '#type' => 'actions'
    ];

    $form['settings']['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Update Name Config'),
      '#markup' => t('<br />')
    ];

    return $form;
  }

  /**
   * Validate the form entry
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $field = $form_state->getValues();
    $firstName = $field['first_name'];
    $lastName = $field['last_name'];

    if (!trim($firstName)) {
      $form_state->setErrorByName('first_name', t('Please enter a first name'));
    } else if (!trim($lastName)) {
      $form_state->setErrorByName('last_name', t('Please enter a last name'));
    }
  }

  /**
   * Submit form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function submitForm( array &$form, FormStateInterface $form_state ) {
    $messenger = \Drupal::messenger();

    $field = $form_state->getValues();
    $firstName = $field['first_name'];
    $lastName = $field['last_name'];

    try {
      \Drupal::configFactory()->getEditable('velir_custom_namer.settings')
         ->set('first_name', $firstName)
         ->set('last_name', $lastName)
         ->save();

      $messenger->addMessage(t('Namer Config updated successfully!'));
    } catch (\Exception $e) {
      $messenger->addWarning(t('There was a problem updating the config: ' . $e->getMessage()));
    }
  }

}
