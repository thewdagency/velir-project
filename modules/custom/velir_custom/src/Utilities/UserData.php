<?php
/**
 * @file
 * Contains \velir_services\Utilities\UserData
 */
namespace Drupal\velir_custom\Utilities;

class UserData {
  /**
   * Current User Object
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  public function __construct($currentUser) {
    $this->setCurrentUser($currentUser);
  }

  /**
   * @return \Drupal\Core\Session\AccountProxyInterface
   */
  public function getCurrentUser(): \Drupal\Core\Session\AccountProxyInterface {
    return $this->currentUser;
  }

  /**
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   */
  public function setCurrentUser( $currentUser ) {
    $this->currentUser = $currentUser;
  }

  /**
   * @return array
   */
  public function getCurrentUserRoles(): array {
    return $this->currentUser->getRoles();
  }

  /**
   * @return int
   */
  public function getCurrentUserId(): int {
    return $this->currentUser->id();
  }
}
